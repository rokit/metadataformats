package metadataformats

import (
	"encoding/xml"
	"time"
)

type DateStamp struct {
	time.Time
}

func (ds DateStamp) String() string {
	return ds.UTC().Format(time.RFC3339)
}

func (dt DateStamp) MarshalXML(enc *xml.Encoder, start xml.StartElement) error {
	return enc.EncodeElement(dt.UTC().Format(time.RFC3339), start)
}

func (dt *DateStamp) UnmarshalXML(dec *xml.Decoder, start xml.StartElement) error {
	var val string
	dec.DecodeElement(&val, &start)
	parsed, _, err := ParseTime(val)
	if err != nil {
		return err
	}
	*dt = DateStamp{*parsed}
	return nil
}

func ParseTime(s string) (*time.Time, bool, error) {
	dateTimeGranularity := true
	parsed, err := time.Parse(time.RFC3339, s)
	if err != nil {
		dateTimeGranularity = false
		parsed, err = time.Parse("2006-01-02", s)
		if err != nil {
			return nil, dateTimeGranularity, err
		}
	}
	return &parsed, dateTimeGranularity, nil
}
