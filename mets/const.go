package mets

type mdType string

const (
	MD_MARC          mdType = "MARC"
	MD_MODS          mdType = "MODS"
	MD_EAD           mdType = "EAD"
	MD_DC            mdType = "DC"
	MD_NISOIMG       mdType = "NISOIMG"
	MD_LC            mdType = "LC-AV"
	MD_VRA           mdType = "VRA"
	MD_TEIHDR        mdType = "TEIHDR"
	MD_DDI           mdType = "DDI"
	MD_FGDC          mdType = "FGDC"
	MD_LOM           mdType = "LOM"
	MD_PREMIS        mdType = "PREMIS"
	MD_PREMIS_OBJECT mdType = "PREMIS:OBJECT"
	MD_PREMIS_AGENT  mdType = "PREMIS:AGENT"
	MD_PREMIS_RIGHTS mdType = "PREMIS:RIGHTS"
	MD_PREMIS_EVENT  mdType = "PREMIS:EVENT"
	MD_TEXTMD        mdType = "TEXTMD"
	MD_METSRIGHTS    mdType = "METSRIGHTS"
	MD_ISO_NAP       mdType = "ISO 19115:2003 NAP"
	MD_EAC_CPF       mdType = "EAC-CPF"
	MD_LIDO          mdType = "LIDO"
	MD_OTHER         mdType = "OTHER"
)

type locType string

const (
	LOC_ARK    locType = "ARK"
	LOC_URN    locType = "URN"
	LOC_URL    locType = "URL"
	LOC_PURL   locType = "PURL"
	LOC_HANDLE locType = "HANDLE"
	LOC_DOI    locType = "DOI"
	LOC_OTHER  locType = "OTHER"
)

type extType string

const (
	EXTTYPE_BYTE           extType = "BYTE"
	EXTTYPE_SMIL           extType = "SMIL"
	EXTTYPE_MIDI           extType = "MIDI"
	EXTTYPE_SMPTE_25       extType = "SMPTE-25"
	EXTTYPE_SMPTE_24       extType = "SMPTE-24"
	EXTTYPE_SMPTE_DF30     extType = "SMPTE-DF30"
	EXTTYPE_SMPTE_NDF30    extType = "SMPTE-NDF30"
	EXTTYPE_SMPTE_DF29_97  extType = "SMPTE-DF29.97"
	EXTTYPE_SMPTE_NDF29_97 extType = "SMPTE-NDF29.97"
	EXTTYPE_TIME           extType = "TIME"
	EXTTYPE_TCF            extType = "TCF"
)

type shape string

const (
	SHAPE_RECT   shape = "RECT"
	SHAPE_CIRCLE shape = "CIRCLE"
	SHAPE_POLY   shape = "POLY"
)

type beType string

const (
	BETYPE_BYTE           beType = "BYTE"
	BETYPE_IDREF          beType = "IDREF"
	BETYPE_SMIL           beType = "SMIL"
	BETYPE_MIDI           beType = "MIDI"
	BETYPE_SMPTE_25       beType = "SMPTE-25"
	BETYPE_SMPTE_24       beType = "SMPTE-24"
	BETYPE_SMPTE_DF30     beType = "SMPTE-DF30"
	BETYPE_SMPTE_NDF30    beType = "SMPTE-NDF30"
	BETYPE_SMPTE_DF29_97  beType = "SMPTE-DF29.97"
	BETYPE_SMPTE_NDF29_97 beType = "SMPTE-NDF29.97"
	BETYPE_TIME           beType = "TIME"
	BETYPE_TCF            beType = "TCF"
	BETYPE_XPTR           beType = "XPTR"
)

type agentRole string

const (
	ROLE_CREATOR      agentRole = "CREATOR"
	ROLE_EDITOR       agentRole = "EDITOR"
	ROLE_ARCHIVIST    agentRole = "ARCHIVIST"
	ROLE_PRESERVATION agentRole = "PRESERVATION"
	ROLE_DISSEMINATOR agentRole = "DISSEMINATOR"
	ROLE_CUSTODIAN    agentRole = "CUSTODIAN"
	ROLE_IPOWNER      agentRole = "IPOWNER"
	ROLE_OTHER        agentRole = "OTHER"
)

type arcLinkOrder string

const (
	ARC_LINK_ORDERED   arcLinkOrder = "ordered"
	ARC_LINK_UNORDERED arcLinkOrder = "unordered"
)
