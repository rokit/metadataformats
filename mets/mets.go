package mets

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats"
	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

const SCHEMA_LINK = "https://www.loc.gov/standards/mets/mets.xsd"
const NAMESPACE = "http://www.loc.gov/METS/"
const PREFIX = "mets"

type Metadata struct {
	XMLName      xml.Name       `xml:"http://www.loc.gov/METS/ mets"`
	Id           string         `xml:"ID,attr,omitempty"`
	ObjId        string         `xml:"OBJID,attr,omitempty"`
	Label        string         `xml:"LABEL,attr,omitempty"`
	Type         string         `xml:"TYPE,attr,omitempty"`
	Profile      string         `xml:"PROFILE,attr,omitempty"`
	MetsHdr      *MetsHdr       `xml:"metsHdr"`
	DmdSecs      []*MdSec       `xml:"dmdSec,omitempty"`
	AmdSecs      []*AmdSec      `xml:"amdSec,omitempty"`
	FileSec      *FileSec       `xml:"fileSec"`
	StructMaps   []*StructMap   `xml:"structMap"`
	StructLink   *StructLink    `xml:"structLink"`
	BevaviorSecs []*BehaviorSec `xml:"behaviorSec,omitempty"`
}

type MetsHdr struct {
	Id             string                     `xml:"ID,attr,omitempty"`
	AdmId          string                     `xml:"ADMID,attr,omitempty"`
	CreateDate     *metadataformats.DateStamp `xml:"CREATEDATE,attr,omitempty"`
	LastModDate    *metadataformats.DateStamp `xml:"LASTMODDATE,attr,omitempty"`
	RecordStatus   string                     `xml:"RECORDSTATUS,attr,omitempty"`
	Agents         []*Agent                   `xml:",omitempty"`
	AltRecordIDs   []*AltRecordID             `xml:",omitempty"`
	MetsDocumentID *MetsDocumentID            `xml:",omitempty"`
}

type AmdSec struct {
	Id          string   `xml:"ID,attr,omitempty"`
	TechMDs     []*MdSec `xml:"techMD,omitempty"`
	RightsMDs   []*MdSec `xml:"rightsMD,omitempty"`
	SourceMDs   []*MdSec `xml:"sourceMD,omitempty"`
	DigiProvMDs []*MdSec `xml:"digiprovMD,omitempty"`
}

type FileSec struct {
	Id       string     `xml:"ID,attr,omitempty"`
	FileGrps []*FileGrp `xml:"fileGrp,omitempty"`
}

type StructMap struct {
	Id    string `xml:"ID,attr,omitempty"`
	Type  string `xml:"TYPE,attr,omitempty"`
	Label string `xml:"LABEL,attr,omitempty"`
	Divs  []*Div `xml:"div"`
}

type StructLink struct {
	Id         string       `xml:"ID,attr,omitempty"`
	SmLinks    []*SmLink    `xml:"smLink,omitempty"`
	SmLinkGrps []*SmLinkGrp `xml:"smLinkGrp,omitempty"`
}

type BehaviorSec struct {
	Id           string                     `xml:"ID,attr,omitempty"`
	Created      *metadataformats.DateStamp `xml:"CREATED,attr,omitempty"`
	Label        string                     `xml:"LABEL,attr,omitempty"`
	BehaviorSecs []*BehaviorSec             `xml:"behaviorSec,omitempty"`
	Behaviors    []*Behavior                `xml:"behavior,omitempty"`
}

type Behavior struct {
	Id       string                     `xml:"ID,attr,omitempty"`
	StructId string                     `xml:"STRUCTID,attr,omitempty"`
	BType    string                     `xml:"BTYPE,attr,omitempty"`
	Created  *metadataformats.DateStamp `xml:"CREATED,attr,omitempty"`
	Label    string                     `xml:"LABEL,attr,omitempty"`
	GroupId  string                     `xml:"GROUPID,attr,omitempty"`
	AdmId    string                     `xml:"ADMID,attr,omitempty"`
	Body     []*ObjectSequence          `xml:",innerxml"`
}

type ObjectSequence struct {
	InterfaceDef *Object `xml:"interfaceDef"`
	Mechanism    Object  `xml:"mechanism"`
}

type Object struct {
	LocationAttributeGroup
	xlink.SimpleLinkAttributeGroup
	Id    string `xml:"ID,attr,omitempty"`
	Label string `xml:"LABEL,attr,omitempty"`
}

type MdSec struct {
	Id      string                     `xml:"ID,attr"`
	GroupId string                     `xml:"GROUPID,attr,omitempty"`
	AdmId   string                     `xml:"ADMID,attr,omitempty"`
	Created *metadataformats.DateStamp `xml:"CREATED,attr,omitempty"`
	Status  string                     `xml:"STATUS,attr,omitempty"`
	MdRef   *MdRef                     `xml:"mdRef"`
	MdWrap  *MdWrap                    `xml:"mdWrap"`
}

type MdRef struct {
	LocationAttributeGroup
	MetadataAttributeGroup
	FileCoreAttributeGroup
	Id    string `xml:"ID,attr,omitempty"`
	Label string `xml:"LABEL,attr,omitempty"`
	Xptr  string `xml:"XPTR,attr,omitempty"`
}

type MdWrap struct {
	Id    string `xml:"ID,attr,omitempty"`
	Label string `xml:"LABEL,attr,omitempty"`
	MetadataAttributeGroup
	FileCoreAttributeGroup
	BinData string   `xml:"binData,omitempty"`
	XmlData *XmlData `xml:"xmlData"`
}

type XmlData struct {
	Body any `xml:",innerxml"`
}

type LocationAttributeGroup struct {
	LocType locType `xml:"LOCTYPE,attr"`
}

type MetadataAttributeGroup struct {
	MdType        mdType `xml:"MDTYPE,attr"`
	OtherMdType   string `xml:"OTHERMDTYPE,attr,omitempty"`
	MdTypeVersion string `xml:"MDTYPEVERSION,attr,omitempty"`
}

type FileCoreAttributeGroup struct {
	MimeType     string                     `xml:"MIMETYPE,attr,omitempty"`
	Size         int64                      `xml:"SIZE,attr,omitempty"`
	Created      *metadataformats.DateStamp `xml:"CREATED,attr,omitempty"`
	CheckSum     string                     `xml:"CHECKSUM,attr,omitempty"`
	CheckSumType string                     `xml:"CHECKSUMTYPE,attr,omitempty"`
}

type OrderLabelsAttributeGroup struct {
	Order      int    `xml:"ORDER,attr,omitempty"`
	OrderLabel string `xml:"ORDERLABEL,attr,omitempty"`
	Label      string `xml:"LABEL,attr,omitempty"`
}

type Agent struct {
	XMLName   xml.Name  `xml:"agent"`
	Name      string    `xml:"name"`
	Notes     []string  `xml:"note,omitempty"`
	Id        string    `xml:"ID,attr,omitempty"`
	Role      agentRole `xml:"ROLE,attr"`
	OtherRole string    `xml:"OTHERROLE,attr,omitempty"`
	Type      string    `xml:"TYPE,attr,omitempty"`
	OtherType string    `xml:"OTHERTYPE,attr,omitempty"`
}

type AltRecordID struct {
	XMLName xml.Name `xml:"altRecordID"`
	Id      string   `xml:"ID,attr,omitempty"`
	Type    string   `xml:"TYPE,attr,omitempty"`
}

type MetsDocumentID struct {
	XMLName xml.Name `xml:"metsDocumentID"`
	Id      string   `xml:"ID,attr,omitempty"`
	Type    string   `xml:"TYPE,attr,omitempty"`
}

type FileGrp struct {
	Id       string                     `xml:"ID,attr,omitempty"`
	VersDate *metadataformats.DateStamp `xml:"VERSDATE,attr,omitempty"`
	AdmId    string                     `xml:"ADMID,attr,omitempty"`
	Use      string                     `xml:"USE,attr,omitempty"`
	FileGrps []*FileGrp                 `xml:"fileGrp,omitempty"`
	Files    []*File                    `xml:"file,omitempty"`
}

type File struct {
	FileCoreAttributeGroup
	Id             string    `xml:"ID,attr"`
	Seq            int       `xml:"SEQ,attr,omitempty"`
	OwnerId        string    `xml:"OWNERID,attr,omitempty"`
	AdmId          string    `xml:"ADMID,attr,omitempty"`
	DmdId          string    `xml:"DMDID,attr,omitempty"`
	GroupId        string    `xml:"GROUPID,attr,omitempty"`
	Use            string    `xml:"USE,attr,omitempty"`
	Begin          string    `xml:"BEGIN,attr,omitempty"`
	End            string    `xml:"END,attr,omitempty"`
	BeType         string    `xml:"BETYPE,attr,omitempty"`
	FLocats        []*FLocat `xml:",omitempty"`
	FContent       *FContent
	Streams        []*Stream        `xml:"stream,omitempty"`
	TransformFiles []*TransformFile `xml:"transformFile,omitempty"`
	Files          []*File          `xml:"file,omitempty"`
}

type FLocat struct {
	XMLName xml.Name `xml:"FLocat"`
	LocationAttributeGroup
	xlink.SimpleLinkAttributeGroup
	Id  string `xml:"ID,attr,omitempty"`
	Use string `xml:"USE,attr,omitempty"`
}

type FContent struct {
	XMLName xml.Name `xml:"FContent"`
	LocationAttributeGroup
	Id      string   `xml:"ID,attr,omitempty"`
	Use     string   `xml:"USE,attr,omitempty"`
	BinData string   `xml:"binData,omitempty"`
	XmlData *XmlData `xml:"xmlData"`
}

type Stream struct {
	Body []any `xml:",innerxml"`
}

type TransformFile struct {
	Body []*TransformFileInner `xml:",innerxml"`
}

type TransformFileInner struct {
	XMLName            xml.Name
	Id                 string `xml:"ID,attr,omitempty"`
	TransformType      string `xml:"TRANSFORMTYPE,attr"`
	TransformAlgorithm string `xml:"TRANSFORMALGORITHM,attr"`
	TransformKey       string `xml:"TRANSFORMKEY,attr,omitempty"`
	TransformBehavior  string `xml:"TRANSFORMBEHAVIOR,attr"`
	TransformOrder     uint   `xml:"TRANSFORMORDER,attr"`
}

type Div struct {
	OrderLabelsAttributeGroup
	Id         string  `xml:"ID,attr,omitempty"`
	AdmId      string  `xml:"ADMID,attr,omitempty"`
	DmdId      string  `xml:"DMDID,attr,omitempty"`
	Type       string  `xml:"TYPE,attr,omitempty"`
	ContentIds string  `xml:"CONTENTIDS,attr,omitempty"`
	Mptrs      []*Mptr `xml:"mptr,omitempty"`
	Fptrs      []*Fptr `xml:"fptr,omitempty"`
	Divs       []*Div  `xml:"div,omitempty"`
}

type Mptr struct {
	LocationAttributeGroup
	Id         string `xml:"ID,attr,omitempty"`
	ContentIds string `xml:"CONTENTIDS,attr,omitempty"`
}

type Fptr struct {
	LocationAttributeGroup
	Id         string `xml:"ID,attr,omitempty"`
	FileId     string `xml:"FILEID,attr,omitempty"`
	ContentIds string `xml:"CONTENTIDS,attr,omitempty"`
	Par        *Par   `xml:"par"`
	Sec        *Sec   `xml:"sec"`
	Area       *Area  `xml:"area"`
}

type Par struct {
	OrderLabelsAttributeGroup
	Id    string  `xml:"ID,attr,omitempty"`
	Areas []*Area `xml:"area,omitempty"`
	Secs  []*Sec  `xml:"sec,omitempty"`
}

type Sec struct {
	OrderLabelsAttributeGroup
	Id    string  `xml:"ID,attr,omitempty"`
	Areas []*Area `xml:"area,omitempty"`
	Pars  []*Par  `xml:"par,omitempty"`
}

type Area struct {
	OrderLabelsAttributeGroup
	Id         string  `xml:"ID,attr,omitempty"`
	FileId     string  `xml:"FILEID,attr"`
	Shape      shape   `xml:"shape,attr,omitempty"`
	Coords     string  `xml:"COORDS,attr,omitempty"`
	Begin      string  `xml:"BEGIN,attr,omitempty"`
	End        string  `xml:"END,attr,omitempty"`
	BeType     beType  `xml:"BETYPE,attr,omitempty"`
	Extent     string  `xml:"EXTENT,attr,omitempty"`
	ExtType    extType `xml:"EXTTYPE,attr,omitempty"`
	AdmId      string  `xml:"ADMID,attr,omitempty"`
	ContentIds string  `xml:"CONTENTIDS,attr,omitempty"`
}

type SmLink struct {
	xlink.ArcLinkAttributeGroup
	Id string `xml:"ID,attr,omitempty"`
}

type SmLinkGrp struct {
	xlink.ExtendedLinkAttributeGroup
	Id           string               `xml:"ID,attr"`
	ArcLinkOrder arcLinkOrder         `xml:"ARCLINKORDER,attr"`
	Body         []*SmLinkGrpSequence `xml:",innerxml"`
}

type SmLinkGrpSequence struct {
	SmLocatorLinks []*SmLocatorLink `xml:"smLocatorLink"`
	SmArclinks     []*SmArcLink     `xml:"smArkLink"`
}

type SmLocatorLink struct {
	xlink.LocatorLinkAttributeGroup
	Id string `xml:"ID,attr"`
}

type SmArcLink struct {
	xlink.ArcLinkAttributeGroup
	Id      string `xml:"ID,attr"`
	ArcType string `xml:"ARCTYPE,attr"`
	AdmId   string `xml:"ADMID,attr,omitempty"`
}

func NewMetadata() *Metadata {
	return &Metadata{}
}

func (m *Metadata) WithId(id string) *Metadata {
	m.Id = id
	return m
}

func (m *Metadata) WithObjId(id string) *Metadata {
	m.ObjId = id
	return m
}

func (m *Metadata) WithType(typ string) *Metadata {
	m.Type = typ
	return m
}

func (m *Metadata) GetMetsHdr() *MetsHdr {
	if m.MetsHdr == nil {
		m.MetsHdr = &MetsHdr{}
	}
	return m.MetsHdr
}

func (m *Metadata) GetFileSec() *FileSec {
	if m.FileSec == nil {
		m.FileSec = &FileSec{}
	}
	return m.FileSec
}

func (fs *FileSec) WithId(id string) *FileSec {
	fs.Id = id
	return fs
}

func (fs *FileSec) AddFileGrp(use string) *FileGrp {
	fileGrp := &FileGrp{Use: use}
	fs.FileGrps = append(fs.FileGrps, fileGrp)
	return fileGrp
}

func (fg *FileGrp) AddFile(id string) *File {
	file := &File{Id: id}
	fg.Files = append(fg.Files, file)
	return file
}

func (f *File) AddFLocat(typ locType) *FLocat {
	loc := &FLocat{LocationAttributeGroup: LocationAttributeGroup{LocType: typ}}
	loc.SimpleLinkAttributeGroup = xlink.NewSimpleLinkAttributeGroup()
	f.FLocats = append(f.FLocats, loc)
	return loc
}

func (m *Metadata) AddDmdSec(id string) *MdSec {
	dmdSec := &MdSec{Id: id}
	m.DmdSecs = append(m.DmdSecs, dmdSec)
	return dmdSec
}

func (m *Metadata) AddAmdSec(id string) *AmdSec {
	amdSec := &AmdSec{Id: id}
	m.AmdSecs = append(m.AmdSecs, amdSec)
	return amdSec
}

func (m *Metadata) AddStructMap(typ string) *StructMap {
	structMap := &StructMap{Type: typ}
	m.StructMaps = append(m.StructMaps, structMap)
	return structMap
}

func (md *MdSec) SetXmlMdWrap(mdType mdType, body any) *MdWrap {
	md.MdWrap = &MdWrap{
		MetadataAttributeGroup: MetadataAttributeGroup{MdType: mdType},
		XmlData: &XmlData{
			Body: body,
		},
	}
	return md.MdWrap
}

func (amd *AmdSec) AddRightsMD(id string) *MdSec {
	rightsMD := &MdSec{Id: id}
	amd.RightsMDs = append(amd.RightsMDs, rightsMD)
	return rightsMD
}

func (sm *StructMap) AddDiv() *Div {
	div := &Div{}
	sm.Divs = append(sm.Divs, div)
	return div
}

func (div *Div) WithType(typ string) *Div {
	div.Type = typ
	return div
}

func (div *Div) WithId(id string) *Div {
	div.Id = id
	return div
}

func (div *Div) WithAdmId(id string) *Div {
	div.AdmId = id
	return div
}

func (div *Div) WithDmdId(id string) *Div {
	div.DmdId = id
	return div
}

func (f *File) WithId(id string) *File {
	f.Id = id
	return f
}

func (f *File) WithMimeType(value string) *File {
	f.MimeType = value
	return f
}

func (f *File) WithSeq(value int) *File {
	f.Seq = value
	return f
}

func (f *File) WithSize(value int64) *File {
	f.Size = value
	return f
}

func (f *File) WithChecksum(value string) *File {
	f.CheckSum = value
	return f
}

func (f *File) WithChecksumType(value string) *File {
	f.CheckSumType = value
	return f
}

func (f *File) WithAdmId(value string) *File {
	f.AdmId = value
	return f
}

func (f *File) WithUse(value string) *File {
	f.Use = value
	return f
}

func (f *File) WithGroupId(value string) *File {
	f.GroupId = value
	return f
}

func (f *File) AddFile(id string) *File {
	file := &File{Id: id}
	f.Files = append(f.Files, file)
	return file
}

func (loc *FLocat) WithHref(value string) *FLocat {
	loc.XlinkHref = value
	return loc
}

func (hdr *MetsHdr) WithId(value string) *MetsHdr {
	hdr.Id = value
	return hdr
}

func (hdr *MetsHdr) WithAdmId(value string) *MetsHdr {
	hdr.AdmId = value
	return hdr
}

func (hdr *MetsHdr) WithRecordStatus(value string) *MetsHdr {
	hdr.RecordStatus = value
	return hdr
}

func (hdr *MetsHdr) WithCreateDate(value *metadataformats.DateStamp) *MetsHdr {
	hdr.CreateDate = value
	return hdr
}

func (hdr *MetsHdr) WithLastModDate(value *metadataformats.DateStamp) *MetsHdr {
	hdr.LastModDate = value
	return hdr
}

func (hdr *MetsHdr) AddAgent(name string) *Agent {
	agent := &Agent{Name: name}
	hdr.Agents = append(hdr.Agents, agent)
	return agent
}

func (agent *Agent) WithRole(value agentRole) *Agent {
	agent.Role = value
	return agent
}

func (agent *Agent) WithType(value string) *Agent {
	agent.Type = value
	return agent
}

func (agent *Agent) WithOtherType(value string) *Agent {
	agent.OtherType = value
	return agent
}
