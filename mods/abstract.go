package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type Abstract struct {
	StringPlusLanguage
	IDAttributeGroup
	xlink.SimpleLinkAttributeGroup
	AltFormatAttributeGroup

	XMLName      xml.Name `xml:"abstract"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	Type         string   `xml:"type,attr,omitempty"`
	Sharable     noType   `xml:"sharable,attr,omitempty"`
	AltRepGroup  string   `xml:"altRepGroup,attr,omitempty"`
}
