package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type AccessCondition struct {
	ExtensionDefinition
	xlink.SimpleLinkAttributeGroup
	LanguageAttributeGroup
	AltFormatAttributeGroup
	AuthorityAttributeGroup

	XMLName     xml.Name `xml:"accessCondition"`
	AltRepGroup string   `xml:"altRepGroup,attr,omitempty"`
}
