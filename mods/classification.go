package mods

import "encoding/xml"

type Classification struct {
	StringPlusLanguagePlusAuthority
	IDAttributeGroup

	XMLName      xml.Name         `xml:"classification"`
	Edition      string           `xml:"edition,attr,omitempty"`
	DisplayLabel string           `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string           `xml:"altRepGroup,attr,omitempty"`
	Usage        usagePrimaryType `xml:"usage,attr,omitempty"`
	Generator    string           `xml:"generator,attr,omitempty"`
}
