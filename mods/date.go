package mods

type dateEncodingType string
type datePointType string
type dateQualifierType string

const (
	DATE_ENCODING_W3C_DTF  dateEncodingType = "w3cdtf"
	DATE_ENCODING_ISO_8601 dateEncodingType = "iso8601"
	DATE_ENCODING_W3C_MARC dateEncodingType = "marc"
	DATE_ENCODING_TEMPER   dateEncodingType = "temper"
	DATE_ENCODING_EDTF     dateEncodingType = "edtf"

	DATE_QUALIFIER_APPROXIMATE  dateQualifierType = "approximate"
	DATE_QUALIFIER_INFERRED     dateQualifierType = "inferred"
	DATE_QUALIFIER_QUESTIONABLE dateQualifierType = "questionable"

	DATE_POINT_START datePointType = "start"
	DATE_POINT_END   datePointType = "end"
)

type Date struct {
	StringPlusLanguage

	Encoding  dateEncodingType  `xml:"encoding,attr,omitempty"`
	Qualifier dateQualifierType `xml:"qualifier,attr,omitempty"`
	Point     datePointType     `xml:"point,attr,omitempty"`
	KeyDate   yesType           `xml:"keyDate,attr,omitempty"`
	Calendar  string            `xml:"calendar,attr,omitempty"`
}

type DateOther struct {
	Date

	Type string `xml:"type,attr,omitempty"`
}
