package mods

import "encoding/xml"

type Extension struct {
	ExtensionDefinition

	XMLName xml.Name `xml:"extension"`
}

type ExtensionDefinition struct {
	IDAttributeGroup

	DisplayLabel string `xml:"displayLabel,attr,omitempty"`
	Type         string `xml:"type,attr,omitempty"`

	Body any `xml:",innerxml"`
}
