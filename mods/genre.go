package mods

import "encoding/xml"

type Genre struct {
	StringPlusLanguagePlusAuthority
	IDAttributeGroup

	XMLName      xml.Name         `xml:"genre"`
	Type         string           `xml:"type,attr,omitempty"`
	DisplayLabel string           `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string           `xml:"altRepGroup,attr,omitempty"`
	Usage        usagePrimaryType `xml:"usage,attr,omitempty"`
}
