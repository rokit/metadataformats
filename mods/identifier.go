package mods

import "encoding/xml"

type Identifier struct {
	StringPlusLanguage
	IDAttributeGroup

	XMLName      xml.Name `xml:"identifier"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	Type         string   `xml:"type,attr,omitempty"`
	TypeURI      string   `xml:"typeURI,attr,omitempty"`
	Invalid      yesType  `xml:"invalid,attr,omitempty"`
	AltRepGroup  string   `xml:"altRepGroup,attr,omitempty"`
}
