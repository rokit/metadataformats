package mods

import "encoding/xml"

type Language struct {
	IDAttributeGroup
	LanguageAttributeGroup

	XMLName      xml.Name         `xml:"language"`
	ObjectPart   string           `xml:"objectPart,attr,omitempty"`
	DisplayLabel string           `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string           `xml:"altRepGroup,attr,omitempty"`
	Usage        usagePrimaryType `xml:"usage,attr,omitempty"`

	LanguageTerms []*LanguageTerm `xml:",omitempty"`
	ScriptTerms   []*ScriptTerm   `xml:",omitempty"`
}

type LanguageTerm struct {
	StringPlusLanguagePlusAuthority

	XMLName xml.Name       `xml:"languageTerm"`
	Type    codeOrTextType `xml:"type,attr,omitempty"`
}

type ScriptTerm struct {
	StringPlusLanguagePlusAuthority

	XMLName xml.Name       `xml:"scriptTerm"`
	Type    codeOrTextType `xml:"type,attr,omitempty"`
}
