package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type urlAccessType string
type enumerationAndChronologyUnitType string

const (
	ACCESS_PREVIEW           urlAccessType = "preview"
	ACCESS_RAW_OBJECT        urlAccessType = "raw object"
	ACCESS_OBJECT_IN_CONTEXT urlAccessType = "object in context"

	ENUMARATION_AND_CHRONOLOGY_UNIT_1 enumerationAndChronologyUnitType = "1"
	ENUMARATION_AND_CHRONOLOGY_UNIT_2 enumerationAndChronologyUnitType = "2"
	ENUMARATION_AND_CHRONOLOGY_UNIT_3 enumerationAndChronologyUnitType = "3"
)

type Location struct {
	IDAttributeGroup
	LanguageAttributeGroup

	XMLName      xml.Name `xml:"location"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string   `xml:"altRepGroup,attr,omitempty"`

	PhysicalLocations []*PhysicalLocation  `xml:",omitempty"`
	ShelfLocators     []*ShelfLocator      `xml:",omitempty"`
	Urls              []*Url               `xml:",omitempty"`
	HoldingSimple     *HoldingSimple       `xml:",omitempty"`
	HoldingExternal   *ExtensionDefinition `xml:"holdingExternal,omitempty"`
}

type PhysicalLocation struct {
	StringPlusLanguagePlusAuthority
	xlink.SimpleLinkAttributeGroup

	XMLName      xml.Name `xml:"physicalLocation"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	Type         string   `xml:"type,attr,omitempty"`
}

type ShelfLocator struct {
	StringPlusLanguage

	XMLName xml.Name `xml:"shelfLocator"`
}

type HoldingSimple struct {
	XMLName xml.Name `xml:"holdingSimple"`

	CopyInformations []*CopyInformation `xml:",omitempty"`
}

type CopyInformation struct {
	XMLName xml.Name `xml:"copyInformation"`

	Form                       *Form                       `xml:",omitempty"`
	SubLocations               []*SubLocation              `xml:",omitempty"`
	ShelfLocators              []*ShelfLocator             `xml:",omitempty"`
	ElectronicLocators         []*ElectronicLocator        `xml:",omitempty"`
	Notes                      []*CopyInformationNote      `xml:",omitempty"`
	EnumerationAndChronologies []*EnumerationAndChronology `xml:",omitempty"`
	ItemIdentifiers            []*ItemIdentifier           `xml:",omitempty"`
}

type Form struct {
	StringPlusLanguagePlusAuthority

	XMLName xml.Name `xml:"form"`
	Type    string   `xml:"type,attr,omitempty"`
}

type SubLocation struct {
	StringPlusLanguage

	XMLName xml.Name `xml:"subLocation"`
}

type ElectronicLocator struct {
	StringPlusLanguage

	XMLName xml.Name `xml:"electronicLocator"`
}

type EnumerationAndChronology struct {
	StringPlusLanguage

	XMLName  xml.Name                         `xml:"enumerationAndChronology"`
	UnitType enumerationAndChronologyUnitType `xml:"unitType,attr,omitempty"`
}

type Url struct {
	XMLName          xml.Name      `xml:"url"`
	DateLastAccessed string        `xml:"dateLastAccessed,attr,omitempty"`
	DisplayLabel     string        `xml:"displayLabel,attr,omitempty"`
	Note             string        `xml:"note,attr,omitempty"`
	Access           urlAccessType `xml:"access,attr,omitempty"`
	Usage            usageType     `xml:"usage,attr,omitempty"`

	Text string `xml:",chardata"`
}

type CopyInformationNote struct {
	StringPlusLanguage
	IDAttributeGroup
	xlink.SimpleLinkAttributeGroup

	XMLName      xml.Name `xml:"note"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	Type         string   `xml:"type,attr,omitempty"`
}

type ItemIdentifier struct {
	StringPlusLanguage

	XMLName xml.Name `xml:"itemIdentifier"`
	Type    string   `xml:"type,attr,omitempty"`
}

func (l *Location) AddUrl(text string, displayLabel string, dateLastAccessed string) *Url {
	url := &Url{DisplayLabel: displayLabel, DateLastAccessed: dateLastAccessed}
	url.Text = text
	l.Urls = append(l.Urls, url)
	return url
}

func (l *Location) AddPhysicalLocation(text string, displayLabel string, authority string, typ string) *PhysicalLocation {
	physicalLoacation := &PhysicalLocation{DisplayLabel: displayLabel, Type: typ}
	physicalLoacation.WithAuthority(authority).WithText(text)
	l.PhysicalLocations = append(l.PhysicalLocations, physicalLoacation)
	return physicalLoacation
}
