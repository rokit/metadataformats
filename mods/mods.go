package mods

import (
	"encoding/xml"
)

const SCHEMA_LINK = "https://www.loc.gov/standards/mods/v3/mods-3-8.xsd"
const NAMESPACE = "http://www.loc.gov/mods/v3"
const PREFIX = "mods"

type modsVersionType string
type codeOrTextType string
type noType string
type yesType string
type usagePrimaryType string
type usageType usagePrimaryType

const (
	MODS_VERSION_3_0 modsVersionType = "3.0"
	MODS_VERSION_3_1 modsVersionType = "3.1"
	MODS_VERSION_3_2 modsVersionType = "3.2"
	MODS_VERSION_3_3 modsVersionType = "3.3"
	MODS_VERSION_3_4 modsVersionType = "3.4"
	MODS_VERSION_3_5 modsVersionType = "3.5"
	MODS_VERSION_3_6 modsVersionType = "3.6"
	MODS_VERSION_3_7 modsVersionType = "3.7"
	MODS_VERSION_3_8 modsVersionType = "3.8"

	CODE_OR_TEXT_CODE codeOrTextType = "code"
	CODE_OR_TEXT_TEXT codeOrTextType = "text"

	NO  noType  = "no"
	YES yesType = "yes"

	USAGE_PRIMARY         usagePrimaryType = "primary"
	USAGE_PRIMARY_DISPLAY usageType        = "primary display"
)

type Mods struct {
	XMLName xml.Name        `xml:"http://www.loc.gov/mods/v3 mods"`
	Id      string          `xml:"ID,attr,omitempty"`
	Version modsVersionType `xml:"version,attr,omitempty"`

	Abstracts            []*Abstract            `xml:",omitempty"`
	AccessConditions     []*AccessCondition     `xml:",omitempty"`
	Classifications      []*Classification      `xml:",omitempty"`
	Extensions           []*Extension           `xml:",omitempty"`
	Genres               []*Genre               `xml:",omitempty"`
	Identifiers          []*Identifier          `xml:",omitempty"`
	Languages            []*Language            `xml:",omitempty"`
	Locations            []*Location            `xml:",omitempty"`
	Names                []*Name                `xml:",omitempty"`
	Notes                []*Note                `xml:",omitempty"`
	OriginInfos          []*OriginInfo          `xml:",omitempty"`
	Parts                []*Part                `xml:",omitempty"`
	PhysicalDescriptions []*PhysicalDescription `xml:",omitempty"`
	RecordInfos          []*RecordInfo          `xml:",omitempty"`
	RelatedItems         []*RelatedItem         `xml:",omitempty"`
	Subjects             []*Subject             `xml:",omitempty"`
	TableOfContents      []*TableOfContents     `xml:",omitempty"`
	TargetAudiences      []*TargetAudience      `xml:",omitempty"`
	TitleInfos           []*TitleInfo           `xml:",omitempty"`
	TypeOfResources      []*TypeOfResource      `xml:",omitempty"`
}

type ModsCollection struct {
	XMLName xml.Name `xml:"http://www.loc.gov/mods/v3 modsCollection"`

	Mods []*Mods `xml:",omitempty"`
}

type AuthorityAttributeGroup struct {
	Authority    string `xml:"authority,attr,omitempty"`
	AuthorityURI string `xml:"authorityURI,attr,omitempty"`
	ValueURI     string `xml:"valueURI,attr,omitempty"`
}

type LanguageAttributeGroup struct {
	Lang            string `xml:"lang,attr,omitempty"`
	XmlLang         string `xml:"xml:lang,attr,omitempty"`
	Script          string `xml:"script,attr,omitempty"`
	Transliteration string `xml:"transliteration,attr,omitempty"`
}

type AltFormatAttributeGroup struct {
	AltFormat   string `xml:"altFormat,attr,omitempty"`
	ContentType string `xml:"contentType,attr,omitempty"`
}

type IDAttributeGroup struct {
	Id    string `xml:"ID,attr,omitempty"`
	IdRef string `xml:"xml:IDREF,attr,omitempty"`
}

type StringPlusLanguage struct {
	LanguageAttributeGroup

	Text string `xml:",chardata"`
}

func (o *StringPlusLanguage) WithText(text string) *StringPlusLanguage {
	o.Text = text
	return o
}

func (o *StringPlusLanguage) WithLang(lang string) *StringPlusLanguage {
	o.Lang = lang
	return o
}

func (o *StringPlusLanguage) WithXmlLang(lang string) *StringPlusLanguage {
	o.XmlLang = lang
	return o
}

type StringPlusLanguagePlusAuthority struct {
	StringPlusLanguage
	AuthorityAttributeGroup
}

func (o *StringPlusLanguagePlusAuthority) WithAuthority(authority string) *StringPlusLanguagePlusAuthority {
	o.Authority = authority
	return o
}

type StringPlusLanguagePlusSupplied struct {
	StringPlusLanguage

	Supplied yesType `xml:"supplied,attr,omitempty"`
}

func (o *StringPlusLanguagePlusSupplied) WithSupplied() *StringPlusLanguagePlusSupplied {
	o.Supplied = YES
	return o
}

func New() *Mods {
	return &Mods{Version: MODS_VERSION_3_8}
}

func NewCollection() *ModsCollection {
	return &ModsCollection{}
}

func NewLangString(text string, lang string) *StringPlusLanguage {
	s := &StringPlusLanguage{Text: text}
	s.Lang = lang
	return s
}

func NewLangStringArray(lang string, text ...string) []*StringPlusLanguage {
	var result []*StringPlusLanguage
	for _, v := range text {
		s := &StringPlusLanguage{Text: v}
		s.Lang = lang
		result = append(result, s)
	}
	return result
}

func (c *ModsCollection) AddMods() *Mods {
	m := New()
	c.Mods = append(c.Mods, m)
	return m
}

func (m *Mods) WithId(id string) *Mods {
	m.Id = id
	return m
}

func (m *Mods) AddTitleInfo(typ titleInfoType) *TitleInfo {
	titleInfo := &TitleInfo{Type: typ}
	m.TitleInfos = append(m.TitleInfos, titleInfo)
	return titleInfo
}

func (m *Mods) AddName(typ nameType) *Name {
	name := &Name{Type: typ}
	m.Names = append(m.Names, name)
	return name
}

func (m *Mods) AddGenre(text string) *Genre {
	genre := &Genre{}
	genre.WithText(text)
	m.Genres = append(m.Genres, genre)
	return genre
}

func (m *Mods) AddOriginInfo() *OriginInfo {
	originInfo := &OriginInfo{}
	m.OriginInfos = append(m.OriginInfos, originInfo)
	return originInfo
}

func (m *Mods) AddAccessCondition() *AccessCondition {
	accessCondition := &AccessCondition{}
	m.AccessConditions = append(m.AccessConditions, accessCondition)
	return accessCondition
}

func (m *Mods) AddIdentifier(value string, typ string) *Identifier {
	identifier := &Identifier{Type: typ}
	identifier.WithText(value)
	m.Identifiers = append(m.Identifiers, identifier)
	return identifier
}

func (m *Mods) AddLocation() *Location {
	location := &Location{}
	m.Locations = append(m.Locations, location)
	return location
}

func (m *Mods) AddSubject() *Subject {
	subject := &Subject{}
	m.Subjects = append(m.Subjects, subject)
	return subject
}

func (m *Mods) AddRecordInfo() *RecordInfo {
	info := &RecordInfo{}
	m.RecordInfos = append(m.RecordInfos, info)
	return info
}
