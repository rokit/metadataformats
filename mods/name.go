package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type nameType string
type namePartType string

const (
	NAME_PERSONAL   nameType = "personal"
	NAME_CORPORATE  nameType = "corporate"
	NAME_CONFERENCE nameType = "conference"
	NAME_FAMILY     nameType = "family"

	NAME_PART_DATE             namePartType = "date"
	NAME_PART_FAMILY           namePartType = "family"
	NAME_PART_GIVEN            namePartType = "given"
	NAME_PART_TERMS_OF_ADDRESS namePartType = "termsOfAddress"
)

type Name struct {
	IDAttributeGroup
	AuthorityAttributeGroup
	xlink.SimpleLinkAttributeGroup
	LanguageAttributeGroup

	XMLName        xml.Name         `xml:"name"`
	DisplayLabel   string           `xml:"displayLabel,attr,omitempty"`
	AltRepGroup    string           `xml:"altRepGroup,attr,omitempty"`
	NameTitleGroup string           `xml:"nameTitleGroup,attr,omitempty"`
	Usage          usagePrimaryType `xml:"usage,attr,omitempty"`
	Type           nameType         `xml:"type,attr,omitempty"`
	Supplied       yesType          `xml:"supplied,attr,omitempty"`

	NameParts        []*NamePart        `xml:",omitempty"`
	DisplayForms     []*DisplayForm     `xml:",omitempty"`
	Affiliations     []*Affiliation     `xml:",omitempty"`
	Roles            []*Role            `xml:",omitempty"`
	Descriptions     []*Description     `xml:",omitempty"`
	NameIdentifiers  []*Identifier      `xml:"nameIdentifier,omitempty"`
	AlternativeNames []*AlternativeName `xml:",omitempty"`
}

type NamePart struct {
	StringPlusLanguage

	XMLName xml.Name     `xml:"namePart"`
	Type    namePartType `xml:"type,attr,omitempty"`
}

type DisplayForm struct {
	StringPlusLanguage

	XMLName xml.Name `xml:"displayForm"`
}

type Affiliation struct {
	StringPlusLanguagePlusAuthority

	XMLName xml.Name `xml:"affiliation"`
}

type Role struct {
	XMLName xml.Name `xml:"role"`

	RoleTerms []*RoleTerm `xml:",omitempty"`
}

type RoleTerm struct {
	StringPlusLanguagePlusAuthority

	XMLName xml.Name       `xml:"roleTerm"`
	Type    codeOrTextType `xml:"type,attr,omitempty"`
}

type Description struct {
	StringPlusLanguage

	XMLName xml.Name `xml:"description"`
}

type AlternativeName struct {
	LanguageAttributeGroup
	xlink.SimpleLinkAttributeGroup

	XMLName      xml.Name `xml:"alternativeName"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	AltType      string   `xml:"altType,attr,omitempty"`

	NameParts       []*NamePart    `xml:",omitempty"`
	DisplayForms    []*DisplayForm `xml:",omitempty"`
	Affiliations    []*Affiliation `xml:",omitempty"`
	Roles           []*Role        `xml:",omitempty"`
	Descriptions    []*Description `xml:",omitempty"`
	NameIdentifiers []*Identifier  `xml:"nameIdentifier,omitempty"`
}

func (n *Name) AddNamePart(value string, typ namePartType) *NamePart {
	namePart := &NamePart{Type: typ}
	namePart.WithText(value)
	n.NameParts = append(n.NameParts, namePart)
	return namePart
}

func (n *Name) AddDisplayForm(value string) *DisplayForm {
	displayForm := &DisplayForm{}
	displayForm.WithText(value)
	n.DisplayForms = append(n.DisplayForms, displayForm)
	return displayForm
}

func (n *Name) AddNameIdentifier(value string, typ string, typeURI string) *Identifier {
	identifier := &Identifier{Type: typ, TypeURI: typeURI}
	identifier.WithText(value)
	n.NameIdentifiers = append(n.NameIdentifiers, identifier)
	return identifier
}

func (n *Name) AddRole() *Role {
	role := &Role{}
	n.Roles = append(n.Roles, role)
	return role
}

func (r *Role) AddRoleTerm(body string, authority string, typ codeOrTextType) *RoleTerm {
	roleTerm := &RoleTerm{Type: typ}
	roleTerm.Authority = authority
	roleTerm.Text = body
	r.RoleTerms = append(r.RoleTerms, roleTerm)
	return roleTerm
}
