package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type Note struct {
	StringPlusLanguage
	IDAttributeGroup
	xlink.SimpleLinkAttributeGroup

	XMLName      xml.Name `xml:"note"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	Type         string   `xml:"type,attr,omitempty"`
	TypeURI      string   `xml:"typeURI,attr,omitempty"`
	AltRepGroup  string   `xml:"altRepGroup,attr,omitempty"`
}
