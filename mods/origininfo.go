package mods

import "encoding/xml"

type issuanceType string

const (
	ISSUANCE_CONTINUING           issuanceType = "continuing"
	ISSUANCE_MONOGRAPHIC          issuanceType = "monographic"
	ISSUANCE_SINGLE_UNIT          issuanceType = "single unit"
	ISSUANCE_MULTIPART_MONOGRAPH  issuanceType = "multipart monograph"
	ISSUANCE_SERIAL               issuanceType = "serial"
	ISSUANCE_INTEGRATING_RESOURCE issuanceType = "integrating resource"
)

type OriginInfo struct {
	IDAttributeGroup
	LanguageAttributeGroup

	XMLName      xml.Name `xml:"originInfo"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string   `xml:"altRepGroup,attr,omitempty"`
	EventType    string   `xml:"eventType,attr,omitempty"`
	EventTypeURI string   `xml:"eventTypeURI,attr,omitempty"`

	Places         []*Place                           `xml:",omitempty"`
	Publishers     []*Publisher                       `xml:"publisher,omitempty"`
	DatesIssued    []*Date                            `xml:"dateIssued,omitempty"`
	DatesCreated   []*Date                            `xml:"dateCreated,omitempty"`
	DatesCaptured  []*Date                            `xml:"dateCaptured,omitempty"`
	DatesValid     []*Date                            `xml:"dateValid,omitempty"`
	DatesModified  []*Date                            `xml:"dateModified,omitempty"`
	CopyrightDates []*Date                            `xml:"copyrightDate,omitempty"`
	DatesOther     []*DateOther                       `xml:"dateOther,omitempty"`
	DisplayDates   []string                           `xml:"displayDate,omitempty"`
	Editions       []*StringPlusLanguagePlusSupplied  `xml:"edition,omitempty"`
	Issuances      []issuanceType                     `xml:"issuance,omitempty"`
	Frequencies    []*StringPlusLanguagePlusAuthority `xml:"frequency,omitempty"`
	Agents         []*Name                            `xml:"agent,omitempty"`
}

type Place struct {
	XMLName  xml.Name `xml:"place"`
	Supplied yesType  `xml:"supplied,attr,omitempty"`

	PlaceTerms       []*PlaceTerm     `xml:",omitempty"`
	PlaceIdentifiers []string         `xml:"placeIdentifier,omitempty"`
	Cartographics    []*Cartographics `xml:",omitempty"`
}

type PlaceTerm struct {
	StringPlusLanguagePlusAuthority

	XMLName xml.Name       `xml:"placeTerm"`
	Type    codeOrTextType `xml:"type,attr,omitempty"`
}

type Publisher struct {
	StringPlusLanguagePlusSupplied
	AuthorityAttributeGroup

	XMLName xml.Name `xml:"publisher"`
}

func (oi *OriginInfo) AddDateIssued(date string, encoding dateEncodingType, qualifier dateQualifierType, point datePointType) *OriginInfo {
	d := &Date{Encoding: encoding, Qualifier: qualifier, Point: point}
	d.WithText(date)
	oi.DatesIssued = append(oi.DatesIssued, d)
	return oi
}
