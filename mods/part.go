package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type Part struct {
	LanguageAttributeGroup

	XMLName      xml.Name `xml:"part"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string   `xml:"altRepGroup,attr,omitempty"`
	Type         string   `xml:"type,attr,omitempty"`
	Order        *int     `xml:"order,attr,omitempty"`

	Details []*Detail     `xml:",omitempty"`
	Extents []*PartExtent `xml:",omitempty"`
	Dates   []*Date       `xml:"date,omitempty"`
	Texts   []*Text       `xml:",omitempty"`
}

type Detail struct {
	XMLName xml.Name `xml:"detail"`
	Type    string   `xml:"type,attr,omitempty"`
	Level   *uint    `xml:"level,attr,omitempty"`

	Numbers  []*StringPlusLanguage `xml:"number,omitempty"`
	Captions []*StringPlusLanguage `xml:"captions,omitempty"`
	Titles   []*StringPlusLanguage `xml:"title,omitempty"`
}

type PartExtent struct {
	XMLName xml.Name `xml:"extent"`
	Unit    string   `xml:"unit,attr"`

	Start *StringPlusLanguage `xml:"start,omitempty"`
	End   *StringPlusLanguage `xml:"end,omitempty"`
	Total *uint               `xml:"total,omitempty"`
	List  *StringPlusLanguage `xml:"list,omitempty"`
}

type Text struct {
	StringPlusLanguage
	xlink.SimpleLinkAttributeGroup

	XMLName      xml.Name `xml:"text"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	Type         string   `xml:"type,attr,omitempty"`
}
