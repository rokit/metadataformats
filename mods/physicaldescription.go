package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type reformattingQualityType string
type digitalOriginType string

const (
	REFORMATTING_QUALITY_ACCESS       reformattingQualityType = "access"
	REFORMATTING_QUALITY_PRESERVATION reformattingQualityType = "preservation"
	REFORMATTING_QUALITY_REPLACEMENT  reformattingQualityType = "replacement"

	DIGITAL_ORIGIN_BORN_DIGITAL           digitalOriginType = "born digital"
	DIGITAL_ORIGIN_REFORMATTED_DIGITAL    digitalOriginType = "reformatted digital"
	DIGITAL_ORIGIN_DIGITIZED_MICROFILM    digitalOriginType = "digitized microfilm"
	DIGITAL_ORIGIN_DIGITIZED_OTHER_ANALOG digitalOriginType = "digitized other analog"
)

type PhysicalDescription struct {
	IDAttributeGroup
	LanguageAttributeGroup

	XMLName      xml.Name `xml:"physicalDescription"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string   `xml:"altRepGroup,attr,omitempty"`

	Forms                 []*Form                    `xml:",omitempty"`
	ReformattingQualities []reformattingQualityType  `xml:"reformattingQuality,omitempty"`
	InternetMediaTypes    []*StringPlusLanguage      `xml:"internetMediaType,omitempty"`
	Extents               []*Extent                  `xml:",omitempty"`
	DigitalOrigins        []digitalOriginType        `xml:"digitalOrigin,omitempty"`
	Notes                 []*PhysicalDescriptionNote `xml:"note,omitempty"`
}

type Extent struct {
	StringPlusLanguagePlusSupplied

	XMLName xml.Name `xml:"extent"`
	Unit    string   `xml:"unit,attr"`
}

type PhysicalDescriptionNote struct {
	IDAttributeGroup
	StringPlusLanguage
	xlink.SimpleLinkAttributeGroup

	DisplayLabel string `xml:"displayLabel,attr,omitempty"`
	Type         string `xml:"type,attr,omitempty"`
	TypeUri      string `xml:"typeURI,attr,omitempty"`
}
