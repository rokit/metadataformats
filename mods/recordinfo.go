package mods

import "encoding/xml"

type RecordInfo struct {
	IDAttributeGroup
	LanguageAttributeGroup

	XMLName      xml.Name         `xml:"recordInfo"`
	DisplayLabel string           `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string           `xml:"altRepGroup,attr,omitempty"`
	Usage        usagePrimaryType `xml:"usage,attr,omitempty"`

	RecordContentSources  []*StringPlusLanguagePlusAuthority `xml:",omitempty"`
	RecordCreationDates   []*Date                            `xml:"recordCreationDate,omitempty"`
	RecordChangeDates     []*Date                            `xml:"recordChangeDate,omitempty"`
	RecordIdentifiers     []*RecordIdentifier                `xml:",omitempty"`
	LanguagesOfCataloging []*Language                        `xml:"languageOfCataloging,omitempty"`
	RecordOrigins         []*StringPlusLanguage              `xml:"recordOrigin,omitempty"`
	DescriptionStandards  []*StringPlusLanguagePlusAuthority `xml:"descriptionStandard,omitempty"`
	RecordInfoNotes       []*Note                            `xml:"recordInfoNote,omitempty"`
}

type RecordIdentifier struct {
	StringPlusLanguage

	XMLName xml.Name `xml:"recordIdentifier"`
	Source  string   `xml:"source,attr,omitempty"`
}

func (ri *RecordInfo) AddNote(text string, lang string) *Note {
	note := &Note{}
	note.WithText(text).WithLang(lang)
	ri.RecordInfoNotes = append(ri.RecordInfoNotes, note)
	return note
}
