package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type relatedItemType string

const (
	RELATED_ITEM_PRECEEDING       reformattingQualityType = "preceeding"
	RELATED_ITEM_SUCCEEDING       reformattingQualityType = "succeeding"
	RELATED_ITEM_ORIGINAL         reformattingQualityType = "original"
	RELATED_ITEM_HOST             reformattingQualityType = "host"
	RELATED_ITEM_CONTINUENT       reformattingQualityType = "constituent"
	RELATED_ITEM_SERIES           reformattingQualityType = "series"
	RELATED_ITEM_OTHER_VERSION    reformattingQualityType = "otherVersion"
	RELATED_ITEM_OTHER_FORMAT     reformattingQualityType = "otherFormat"
	RELATED_ITEM_IS_REFERENCED_BY reformattingQualityType = "isReferencedBy"
	RELATED_ITEM_REFERENCES       reformattingQualityType = "references"
	RELATED_ITEM_REVIEW_OF        reformattingQualityType = "reviewOf"
)

type RelatedItem struct {
	Mods
	xlink.SimpleLinkAttributeGroup

	XMLName          xml.Name        `xml:"relatedItem"`
	IdRef            string          `xml:"xml:IDREF,attr,omitempty"`
	OtherType        string          `xml:"otherType,attr,omitempty"`
	OtherTypeAuth    string          `xml:"otherTypeAuth,attr,omitempty"`
	OtherTypeAuthUri string          `xml:"otherTypeAuthURI,attr,omitempty"`
	OtherTypeUri     string          `xml:"otherTypeURI,attr,omitempty"`
	DisplayLabel     string          `xml:"displayLabel,attr,omitempty"`
	Type             relatedItemType `xml:"type,attr,omitempty"`
}
