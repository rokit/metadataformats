package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type Subject struct {
	IDAttributeGroup
	AuthorityAttributeGroup
	LanguageAttributeGroup
	xlink.SimpleLinkAttributeGroup

	XMLName      xml.Name         `xml:"subject"`
	DisplayLabel string           `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string           `xml:"altRepGroup,attr,omitempty"`
	Usage        usagePrimaryType `xml:"usage,attr,omitempty"`

	Topics                  []*StringPlusLanguagePlusAuthority `xml:"topic,omitempty"`
	Geographics             []*StringPlusLanguagePlusAuthority `xml:"geographic,omitempty"`
	Temporals               []*Temporal                        `xml:"temporal,omitempty"`
	TitleInfos              []*SubjectTitleInfo                `xml:",omitempty"`
	Names                   []*SubjectName                     `xml:",omitempty"`
	GeographicCodes         []*StringPlusLanguagePlusAuthority `xml:",omitempty"`
	HierarchicalGeographics []*HierarchicalGeographic          `xml:",omitempty"`
	Cartographics           []*Cartographics                   `xml:"cartographics,omitempty"`
	Occupations             []*StringPlusLanguagePlusAuthority `xml:"occupation,omitempty"`
	Genres                  []*Genre                           `xml:",omitempty"`
}

type Temporal struct {
	AuthorityAttributeGroup
	Date
}

type SubjectTitleInfo struct {
	IDAttributeGroup
	AuthorityAttributeGroup
	xlink.SimpleLinkAttributeGroup
	LanguageAttributeGroup

	XMLName          xml.Name      `xml:"titleInfo"`
	DisplayLabel     string        `xml:"displayLabel,attr,omitempty"`
	Type             titleInfoType `xml:"type,attr,omitempty"`
	OtherType        string        `xml:"otherType,attr,omitempty"`
	OtherTypeAuth    string        `xml:"otherTypeAuth,attr,omitempty"`
	OtherTypeAuthUri string        `xml:"otherTypeAuthURI,attr,omitempty"`
	OtherTypeUri     string        `xml:"otherTypeURI,attr,omitempty"`

	Titles      []*StringPlusLanguage `xml:"title,omitempty"`
	SubTitles   []*StringPlusLanguage `xml:"subTitle,omitempty"`
	PartNumbers []*StringPlusLanguage `xml:"partNumber,omitempty"`
	PartNames   []*StringPlusLanguage `xml:"partName,omitempty"`
	NonSorts    []*NonSort            `xml:"nonSort,omitempty"`
}

type SubjectName struct {
	XMLName xml.Name `xml:"name"`
	Type    nameType `xml:"type,attr,omitempty"`

	NameParts       []*NamePart    `xml:",omitempty"`
	DisplayForms    []*DisplayForm `xml:",omitempty"`
	Affiliations    []*Affiliation `xml:",omitempty"`
	Roles           []*Role        `xml:",omitempty"`
	Descriptions    []*Description `xml:",omitempty"`
	NameIdentifiers []*Identifier  `xml:"nameIdentifier,omitempty"`
}

type HierarchicalGeographic struct {
	AuthorityAttributeGroup

	XMLName xml.Name `xml:"hierarchicalGeographic"`

	ExtraTerrestrialAreas []*HierarchicalPart   `xml:"extraTerrestrialArea,omitempty"`
	Continents            []*HierarchicalPart   `xml:"continent,omitempty"`
	Countries             []*HierarchicalPart   `xml:"country,omitempty"`
	Provinces             []*StringPlusLanguage `xml:"province,omitempty"`
	Regions               []*Region             `xml:",omitempty"`
	States                []*State              `xml:",omitempty"`
	Territories           []*HierarchicalPart   `xml:"territory,omitempty"`
	Counties              []*HierarchicalPart   `xml:"county,omitempty"`
	Cities                []*HierarchicalPart   `xml:"city,omitempty"`
	CitySections          []*CitySection        `xml:",omitempty"`
	Islands               []*HierarchicalPart   `xml:"island,omitempty"`
	Areas                 []*Area               `xml:",omitempty"`
}

type HierarchicalPart struct {
	StringPlusLanguage
	AuthorityAttributeGroup

	Level  string `xml:"level,attr,omitempty"`
	Period string `xml:"period,attr,omitempty"`
}

type Area struct {
	HierarchicalPart

	XMLName  xml.Name `xml:"area"`
	AreaType string   `xml:"areaType,attr,omitempty"`
}

type Region struct {
	HierarchicalPart

	XMLName    xml.Name `xml:"region"`
	RegionType string   `xml:"regionType,attr,omitempty"`
}

type CitySection struct {
	HierarchicalPart

	XMLName         xml.Name `xml:"citySection"`
	CitySectionType string   `xml:"citySectionType,attr,omitempty"`
}

type State struct {
	HierarchicalPart

	XMLName   xml.Name `xml:"state"`
	StateType string   `xml:"stateType,attr,omitempty"`
}

type Cartographics struct {
	XMLName xml.Name `xml:"cartographics"`

	Scale                  *StringPlusLanguage   `xml:"scale,omitempty"`
	Projection             *StringPlusLanguage   `xml:"projection,omitempty"`
	Coordinates            []*StringPlusLanguage `xml:"coordinates,omitempty"`
	CartographicExtensions []*Extension          `xml:"cartographicExtension,omitempty"`
}

func (s *Subject) AddCartographics(coordinates []*StringPlusLanguage, projection *StringPlusLanguage) *Cartographics {
	cartographics := &Cartographics{Coordinates: coordinates, Projection: projection}
	s.Cartographics = append(s.Cartographics, cartographics)
	return cartographics
}
