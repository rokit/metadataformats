package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type TableOfContents struct {
	StringPlusLanguage
	IDAttributeGroup
	AltFormatAttributeGroup
	xlink.SimpleLinkAttributeGroup

	XMLName      xml.Name `xml:"tableOfContents"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	Type         string   `xml:"type,attr,omitempty"`
	AltRepGroup  string   `xml:"altRepGroup,attr,omitempty"`
	Shareable    noType   `xml:"shareable,attr,omitempty"`
}
