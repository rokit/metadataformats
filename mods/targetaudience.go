package mods

import "encoding/xml"

type TargetAudience struct {
	StringPlusLanguagePlusAuthority
	IDAttributeGroup

	XMLName      xml.Name `xml:"targetAudience"`
	DisplayLabel string   `xml:"displayLabel,attr,omitempty"`
	AltRepGroup  string   `xml:"altRepGroup,attr,omitempty"`
}
