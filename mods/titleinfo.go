package mods

import (
	"encoding/xml"

	"gitlab.ulb.tu-darmstadt.de/rokit/metadataformats/xlink"
)

type titleInfoType string
type xmlSpaceType string

const (
	TITLE_INFO_ABBREVIATED titleInfoType = "abbreviated"
	TITLE_INFO_TRANSLATED  titleInfoType = "translated"
	TITLE_INFO_ALTERNATIVE titleInfoType = "alternative"
	TITLE_INFO_UNIFORM     titleInfoType = "uniform"

	XML_SPACE_DEFAULT  xmlSpaceType = "default"
	XML_SPACE_PRESERVE xmlSpaceType = "preserve"
)

type TitleInfo struct {
	IDAttributeGroup
	LanguageAttributeGroup
	AuthorityAttributeGroup
	xlink.SimpleLinkAttributeGroup
	AltFormatAttributeGroup

	XMLName          xml.Name         `xml:"titleInfo"`
	Type             titleInfoType    `xml:"type,attr,omitempty"`
	OtherType        string           `xml:"otherType,attr,omitempty"`
	OtherTypeAuth    string           `xml:"otherTypeAuth,attr,omitempty"`
	OtherTypeAuthUri string           `xml:"otherTypeAuthURI,attr,omitempty"`
	OtherTypeUri     string           `xml:"otherTypeURI,attr,omitempty"`
	Supplied         yesType          `xml:"supplied,attr,omitempty"`
	AltRepGroup      string           `xml:"altRepGroup,attr,omitempty"`
	NameTitleGroup   string           `xml:"nameTitleGroup,attr,omitempty"`
	Usage            usagePrimaryType `xml:"usage,attr,omitempty"`
	DisplayLabel     string           `xml:"displayLabel,attr,omitempty"`

	Titles      []*StringPlusLanguage `xml:"title,omitempty"`
	SubTitles   []*StringPlusLanguage `xml:"subTitle,omitempty"`
	PartNumbers []*StringPlusLanguage `xml:"partNumber,omitempty"`
	PartNames   []*StringPlusLanguage `xml:"partName,omitempty"`
	NonSorts    []*NonSort            `xml:"nonSort,omitempty"`
}

type NonSort struct {
	StringPlusLanguage

	Space xmlSpaceType `xml:"xml:space,attr,omitempty"`
}

func (ti *TitleInfo) WithId(id string) *TitleInfo {
	ti.Id = id
	return ti
}

func (ti *TitleInfo) WithType(typ titleInfoType) *TitleInfo {
	ti.Type = typ
	return ti
}

func (ti *TitleInfo) WithAuthority(authority string) *TitleInfo {
	ti.Authority = authority
	return ti
}

func (ti *TitleInfo) WithDisplayLabel(label string) *TitleInfo {
	ti.DisplayLabel = label
	return ti
}

func (ti *TitleInfo) AddTitle(title *StringPlusLanguage) *TitleInfo {
	ti.Titles = append(ti.Titles, title)
	return ti
}

func (ti *TitleInfo) AddSubTitle(subTitle *StringPlusLanguage) *TitleInfo {
	ti.SubTitles = append(ti.SubTitles, subTitle)
	return ti
}

func (ti *TitleInfo) AddPartNumber(number *StringPlusLanguage) *TitleInfo {
	ti.PartNumbers = append(ti.PartNumbers, number)
	return ti
}

func (ti *TitleInfo) AddPartName(name *StringPlusLanguage) *TitleInfo {
	ti.PartNames = append(ti.PartNames, name)
	return ti
}
