package mods

import "encoding/xml"

type TypeOfResource struct {
	StringPlusLanguagePlusAuthority
	IDAttributeGroup

	XMLName      xml.Name         `xml:"typeOfResource"`
	Collection   yesType          `xml:"collection,attr,omitempty"`
	Manuscript   yesType          `xml:"manuscript,attr,omitempty"`
	Usage        usagePrimaryType `xml:"usage,attr,omitempty"`
	AltRepGroup  string           `xml:"altRepGroup,attr,omitempty"`
	DisplayLabel string           `xml:"displayLabel,attr,omitempty"`
}
