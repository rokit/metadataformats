package oaidc

import (
	"encoding/xml"
)

const SCHEMA_LINK = "https://www.openarchives.org/OAI/2.0/oai_dc.xsd"
const NAMESPACE = "http://www.openarchives.org/OAI/2.0/oai_dc/"
const PREFIX = "oai_dc"

type Metadata struct {
	XMLName      xml.Name `xml:"http://www.openarchives.org/OAI/2.0/oai_dc/ dc"`
	Titles       []string `xml:"dc:title,omitempty"`
	Creators     []string `xml:"dc:creator,omitempty"`
	Subjects     []string `xml:"dc:subject,omitempty"`
	Descriptions []string `xml:"dc:description,omitempty"`
	Publishers   []string `xml:"dc:publisher,omitempty"`
	Contributors []string `xml:"dc:contributor,omitempty"`
	Dates        []string `xml:"dc:date,omitempty"`
	Types        []string `xml:"dc:type,omitempty"`
	Formats      []string `xml:"dc:format,omitempty"`
	Identifiers  []string `xml:"dc:identifier,omitempty"`
	Sources      []string `xml:"dc:source,omitempty"`
	Languages    []string `xml:"dc:language,omitempty"`
	Relations    []string `xml:"dc:relation,omitempty"`
	Coverages    []string `xml:"dc:coverage,omitempty"`
	Rights       []string `xml:"dc:rights,omitempty"`
}
