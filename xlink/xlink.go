package xlink

const SCHEMA_LINK = "http://www.loc.gov/standards/xlink/xlink.xsd"
const NAMESPACE = "http://www.w3.org/1999/xlink"
const PREFIX = "xlink"

type show string
type actuate string

const (
	SHOW_NEW     show = "new"
	SHOW_REPLACE show = "replace"
	SHOW_EMBED   show = "embed"
	SHOW_OTHER   show = "other"
	SHOW_NONE    show = "none"

	ACTUATE_ONLOAD    actuate = "onLoad"
	ACTUATE_ONREQUEST actuate = "onRequest"
	ACTUATE_OTHER     actuate = "other"
	ACTUATE_NONE      actuate = "none"
)

type SimpleLinkAttributeGroup struct {
	XlinkType    string  `xml:"xlink:type,attr,omitempty"`
	XlinkHref    string  `xml:"xlink:href,attr,omitempty"`
	XlinkRole    string  `xml:"xlink:role,attr,omitempty"`
	XlinkArcRole string  `xml:"xlink:arcrole,attr,omitempty"`
	XlinkTitle   string  `xml:"xlink:title,attr,omitempty"`
	XlinkShow    show    `xml:"xlink:show,attr,omitempty"`
	XlinkActuate actuate `xml:"xlink:actuate,attr,omitempty"`
}

type ExtendedLinkAttributeGroup struct {
	XlinkType  string `xml:"xlink:type,attr,omitempty"`
	XlinkHref  string `xml:"xlink:href,attr,omitempty"`
	XlinkRole  string `xml:"xlink:role,attr,omitempty"`
	XlinkTitle string `xml:"xlink:title,attr,omitempty"`
}

type LocatorLinkAttributeGroup struct {
	XlinkType  string `xml:"xlink:type,attr,omitempty"`
	XlinkHref  string `xml:"xlink:href,attr"`
	XlinkRole  string `xml:"xlink:role,attr,omitempty"`
	XlinkTitle string `xml:"xlink:title,attr,omitempty"`
	XlinkLabel string `xml:"xlink:label,attr,omitempty"`
}

type ArcLinkAttributeGroup struct {
	XlinkType    string  `xml:"xlink:type,attr,omitempty"`
	XlinkHref    string  `xml:"xlink:href,attr,omitempty"`
	XlinkArcRole string  `xml:"xlink:arcrole,attr,omitempty"`
	XlinkTitle   string  `xml:"xlink:title,attr,omitempty"`
	XlinkShow    show    `xml:"xlink:show,attr,omitempty"`
	XlinkActuate actuate `xml:"xlink:actuate,attr,omitempty"`
	XlinkFrom    actuate `xml:"xlink:from,attr,omitempty"`
	XlinkTo      actuate `xml:"xlink:to,attr,omitempty"`
}

type ResourceLinkAttributeGroup struct {
	XlinkType  string  `xml:"xlink:type,attr,omitempty"`
	XlinkHref  string  `xml:"xlink:href,attr,omitempty"`
	XlinkRole  string  `xml:"xlink:role,attr,omitempty"`
	XlinkTitle string  `xml:"xlink:title,attr,omitempty"`
	XlinkLabel actuate `xml:"xlink:label,attr,omitempty"`
}

type XlinkTitleLinkAttributeGroup struct {
	XlinkType string `xml:"xlink:type,attr,omitempty"`
	XlinkHref string `xml:"xlink:href,attr,omitempty"`
}

type EmptyLinkAttributeGroup struct {
	XlinkType string `xml:"xlink:type,attr,omitempty"`
	XlinkHref string `xml:"xlink:href,attr,omitempty"`
}

func NewSimpleLinkAttributeGroup() SimpleLinkAttributeGroup {
	return SimpleLinkAttributeGroup{
		XlinkType: "simple",
	}
}

func NewExtendedLinkAttributeGroup() ExtendedLinkAttributeGroup {
	return ExtendedLinkAttributeGroup{
		XlinkType: "extended",
	}
}

func NewLocatorLinkAttributeGroup(href string) LocatorLinkAttributeGroup {
	return LocatorLinkAttributeGroup{
		XlinkType: "locator",
		XlinkHref: href,
	}
}

func NewArcLinkAttributeGroup() ArcLinkAttributeGroup {
	return ArcLinkAttributeGroup{
		XlinkType: "arc",
	}
}

func NewResourceLinkAttributeGroup() ResourceLinkAttributeGroup {
	return ResourceLinkAttributeGroup{
		XlinkType: "resource",
	}
}

func NewXlinkTitleLinkAttributeGroup() XlinkTitleLinkAttributeGroup {
	return XlinkTitleLinkAttributeGroup{
		XlinkType: "title",
	}
}

func NewEmptyLinkAttributeGroup() EmptyLinkAttributeGroup {
	return EmptyLinkAttributeGroup{
		XlinkType: "none",
	}
}
